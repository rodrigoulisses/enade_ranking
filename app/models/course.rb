class Course < ApplicationRecord
  belongs_to :university

  has_one :performance_evaluation, as: :evaluable

  accepts_nested_attributes_for :performance_evaluation

  delegate :score_course, :average_score_student, to: :performance_evaluation, allow_nil: true

  validates :name, presence: true

  def to_s
    name
  end
end
