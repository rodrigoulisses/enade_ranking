class PerformanceEvaluation < ApplicationRecord
  belongs_to :evaluable, polymorphic: true, optional: true

  scope :university_name_like, -> (name) { 
    joins("join universities as u on u.id = evaluable_id and evaluable_type = 'University' ").where("u.name like ?", "%#{name}%") }
  scope :course_name_like, -> (name) { 
    joins("join courses as c on c.id = evaluable_id and evaluable_type = 'Course' ").where("c.name like ?", "%#{name}%") }

  validates :score, presence: true, if: :university?
  validates :score_course, :average_score_student, presence: true, if: :course?

  def university?
    evaluable.is_a? University
  end

  def course?
    evaluable.is_a? Course
  end

  def self.ransackable_scopes(auth_object = nil)
    [:course_name_like, :university_name_like]
  end
end
