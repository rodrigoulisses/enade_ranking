class University < ApplicationRecord
  has_one :performance_evaluation, as: :evaluable

  has_many :courses

  accepts_nested_attributes_for :performance_evaluation

  delegate :score, to: :performance_evaluation, allow_nil: true

  validates :name, presence: true

  def to_s
    name
  end
end
