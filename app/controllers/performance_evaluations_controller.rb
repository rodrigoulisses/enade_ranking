class PerformanceEvaluationsController < ApplicationController
  def index
    @query = PerformanceEvaluation.ransack(params[:q])
    @performance_evaluations = @query.result.order('score desc, score_course desc, average_score_student desc').decorate
  end
end
