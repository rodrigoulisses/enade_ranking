class PerformanceEvaluationDecorator < Draper::Decorator
  delegate_all

  def university
    object.university? ? object.evaluable.name : object.evaluable.university.name
  end

  def course
    object.course? ? object.evaluable.name : '-'
  end

  def score
    object.university? ? super : '-'
  end

  def score_course
    object.university? ? '-' : super
  end

  def average_score_student
    object.university? ? '-' : super
  end
end
