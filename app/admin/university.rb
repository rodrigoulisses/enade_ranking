ActiveAdmin.register University do
  permit_params :name, performance_evaluation_attributes: [:score, :id]

  show do
    attributes_table do
      row :name
      row :score
    end
  end

  filter :name

  form do |f|
    f.inputs do
      f.input :name
    end
    f.inputs PerformanceEvaluation.model_name.human, for: [:performance_evaluation, f.object.performance_evaluation || PerformanceEvaluation.new(evaluable: f.object)] do |p|
      p.input :score
    end

    f.actions
  end
end
