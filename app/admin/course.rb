ActiveAdmin.register Course do
  permit_params :name, :university_id, performance_evaluation_attributes: [:score_course, :average_score_student, :id]

  show do
    attributes_table do
      row :university
      row :name
      row :score_course
      row :average_score_student
    end
  end

  filter :name
  filter :university

  form do |f|
    f.inputs Course.model_name.human do
      f.input :university
      f.input :name
    end

    f.inputs PerformanceEvaluation.model_name.human, for: [:performance_evaluation, f.object.performance_evaluation || PerformanceEvaluation.new(evaluable: f.object)]  do |p|
      p.input :score_course
      p.input :average_score_student
    end

    f.actions
  end
end
