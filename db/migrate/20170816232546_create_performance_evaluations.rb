class CreatePerformanceEvaluations < ActiveRecord::Migration[5.1]
  def change
    create_table :performance_evaluations do |t|
      t.references :evaluable, polymorphic: true, index: false
      t.decimal :score, precision: 5, scale: 2, index: true
      t.decimal :score_course, precision: 5, scale: 2, index: true
      t.decimal :average_score_student, precision: 5, scale: 2, index: true

      t.timestamps
    end

    add_index :performance_evaluations, [:evaluable_type, :evaluable_id], name: 'index_perform_evaluations_on_evaluable_type_and_evaluable_id'
  end
end
