class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :name, null: false
      t.references :university, null: false, index: true, foreign_key: true

      t.timestamps
    end
  end
end
