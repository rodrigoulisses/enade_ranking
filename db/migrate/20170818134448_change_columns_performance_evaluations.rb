class ChangeColumnsPerformanceEvaluations < ActiveRecord::Migration[5.1]
  def change
    change_column :performance_evaluations, :score, :decimal, default: 0.0, precision: 12, scale: 2
    change_column :performance_evaluations, :score_course, :decimal, default: 0.0, precision: 12, scale: 2
    change_column :performance_evaluations, :average_score_student, :decimal, default: 0.0, precision: 12, scale: 2
  end
end
