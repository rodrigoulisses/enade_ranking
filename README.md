# Enade Ranking

This is an app to registration the score evaluation of Universities and College Schools.

## Dependencies
 - Ruby 2.4.1
 - Postgres
 - Foreman

## Setup

#### Clone this project
  ```shell
  git clone git@gitlab.com:rodrigoulisses/enade_ranking.git
  ```

#### Install Foreman
  ```shell
  gem install foreman
  ```

#### Run bundle
  ```shell
  bundle
  ```

#### Setup database
  ```shell
  rake db:create && rake db:migrate && rake db:seed
  ```

#### Start server
  ```shell
  foreman start -f Procfile.development
  ```

#### Run tests
  ```shell
  rspec spec/
  ```

