require 'rails_helper'

RSpec.describe PerformanceEvaluation do
  context 'relatioships' do
    it { is_expected.to belong_to(:evaluable) }
  end

  context 'validations' do
    context 'when performance evaluation is university' do
      subject { described_class.new evaluable: create(:university) }

      it { is_expected.to validate_presence_of(:score) }

      it { is_expected.not_to validate_presence_of(:score_course) }
      it { is_expected.not_to validate_presence_of(:average_score_student) }
    end

    context 'when performance evaluation is course' do
      subject { described_class.new evaluable: create(:course) }

      it { is_expected.not_to validate_presence_of(:score) }

      it { is_expected.to validate_presence_of(:score_course) }
      it { is_expected.to validate_presence_of(:average_score_student) }
    end
  end

  describe '#university?' do
    context 'when evaluable is University' do
      subject { described_class.new evaluable: create(:university) }

      it { is_expected.to be_university }
      it { is_expected.not_to be_course }
    end

    context 'when evaluable is Course' do
      subject { described_class.new evaluable: create(:course) }

      it { is_expected.not_to be_university }
      it { is_expected.to be_course }
    end
  end

  context 'scopes' do
    let!(:ufpi) { create(:university, name: 'UFPI') }
    let!(:ufpe) { create(:university, name: 'UFPE') }
    let!(:ufce) { create(:university, name: 'UFCE') }

    describe '#university_name_like' do
      let!(:performance_ufpi) { create(:performance_evaluation, evaluable: ufpi, score: 10) }
      let!(:performance_ufpe) { create(:performance_evaluation, evaluable: ufpe, score: 100) }
      let!(:performance_ufce) { create(:performance_evaluation, evaluable: ufce, score: 60) }

      it 'includes performance_ufpi and performance_ufpe ' do
        expect(described_class.university_name_like('UFP')).to include performance_ufpi, performance_ufpe
      end

      it 'does not includes performance_ufce' do
        expect(described_class.university_name_like('UFP')).not_to include performance_ufce
      end
    end

    describe '#course_name_like' do
      let!(:bcc) { create(:course, name: 'Bach. Ciência da Computação', university: ufpi) }
      let!(:mat) { create(:course, name: 'BCC', university: ufce) }
      let!(:enf) { create(:course, name: 'BCC', university: ufpi) }
      let!(:med) { create(:course, name: 'Ciências Médicas', university: ufpe) }
      let!(:bio) { create(:course, name: 'Ciências Biologicas', university: ufce) }

      let!(:performance_bcc) { create(:performance_evaluation, evaluable: bcc, score_course: 39.89, average_score_student: 40.0) }
      let!(:performance_med) { create(:performance_evaluation, evaluable: med, score_course: 99.99, average_score_student: 99.0) }
      let!(:performance_mat) { create(:performance_evaluation, evaluable: mat, score_course: 24.09, average_score_student: 30.76) }
      let!(:performance_enf) { create(:performance_evaluation, evaluable: enf, score_course: 54.90, average_score_student: 60.90) }
      let!(:performance_bio) { create(:performance_evaluation, evaluable: bio, score_course: 4, average_score_student: 9) }

      it 'includes performance_ufpi and performance_ufpe ' do
        expect(described_class.course_name_like('Ciência')).to include performance_bcc, performance_med, performance_bio
      end

      it 'does not includes performance_ufce' do
        expect(described_class.course_name_like('Ciência')).not_to include performance_mat, performance_enf
      end
    end
  end
end
