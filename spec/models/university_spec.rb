require 'rails_helper'

RSpec.describe University do
  context 'relationships' do
    it { is_expected.to have_many :courses }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of :name }
  end

  context 'delegates' do
    it { is_expected.to delegate_method(:score).to(:performance_evaluation)}
  end

  describe '#to_s' do
    before do
      allow(subject).to receive(:name).and_return('Faculdade Piauiense')
    end

    it 'returns name' do
      expect(subject.to_s).to eql 'Faculdade Piauiense'
    end
  end
end
