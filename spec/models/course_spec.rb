require 'rails_helper'

RSpec.describe Course do
  context 'relationships' do
    it { is_expected.to belong_to :university }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of :name }
  end

  describe '#to_s' do
    before do
      allow(subject).to receive(:name).and_return('Ciência da Computação')
    end

    it 'returns name' do
      expect(subject.to_s).to eql 'Ciência da Computação'
    end
  end
end
