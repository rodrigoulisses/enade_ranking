require 'rails_helper'

RSpec.feature University do
  before do
    login_as create(:admin_user)
  end

  scenario 'create, update and destroy university' do
    visit '/admin'

    click_on 'Universidades'

    click_on 'Novo(a) Universidade'

    fill_in 'Nome', with: 'Universidade Federal do Piauí'
    fill_in 'Nota geral', with: '100.0'
    click_on 'Criar Universidade'

    expect(page).to have_content 'Universidade Federal do Piauí'
    expect(page).to have_content '100.0'
    click_on 'Editar Universidade'

    expect(page).to have_field 'Nome', with: 'Universidade Federal do Piauí'
    expect(page).to have_field 'Nota geral', with: '100.0'

    fill_in 'Nome', with: 'Universidade Federal do Piauí - UFPI'
    fill_in 'Nota geral', with: '90.5'
    click_on 'Atualizar Universidade'

    expect(page).to have_content 'Universidade Federal do Piauí - UFPI'
    expect(page).to have_content '90.5'

    click_on 'Remover Universidade'

    expect(page).not_to have_content 'Universidade Federal do Piauí - UFPI'
  end
end
