require 'rails_helper'

RSpec.feature Course do
  given!(:university) { create(:university) }
  given!(:university2) { create(:university, name: 'Universidade Federal do Ceará') }

  before do
    login_as create(:admin_user)
  end

  scenario 'create, update and destroy course' do
    visit '/admin'

    click_on 'Cursos'

    click_on 'Novo(a) Curso'

    fill_in 'Nome', with: 'Ciências da Computação'
    select university.name, from: 'Universidade'
    fill_in 'Nota do curso', with: '55.0'
    fill_in 'Média dos alunos do curso', with: '40.9'
    click_on 'Criar Curso'

    expect(page).to have_content 'Ciências da Computação'
    expect(page).to have_content university.name
    expect(page).to have_content 55.0
    expect(page).to have_content 40.9

    click_on 'Editar Curso'

    expect(page).to have_field 'Nome', with: 'Ciências da Computação'
    expect(page).to have_select 'Universidade', selected: university.name
    expect(page).to have_field 'Nota do curso', with: '55.0'
    expect(page).to have_field 'Média dos alunos do curso', with: '40.9'

    fill_in 'Nome', with: 'Ciência da Computação'
    select university2.name, from: 'Universidade'
    fill_in 'Nota do curso', with: '50.0'
    fill_in 'Média dos alunos do curso', with: '400.9'

    click_on 'Atualizar Curso'

    expect(page).to have_content 'Ciência da Computação'
    expect(page).to have_content university2.name

    click_on 'Remover Curso'

    expect(page).not_to have_content 'Ciência da Computação'
  end
end
