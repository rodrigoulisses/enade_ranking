require 'rails_helper'

RSpec.feature PerformanceEvaluation do
  given!(:ufpi) { create(:university, name: 'UFPI') }
  given!(:ufpe) { create(:university, name: 'UFPE') }
  given!(:ufpb) { create(:university, name: 'UFPB') }

  given!(:bcc) { create(:course, name: 'Ciência da Computação', university: ufpi) }
  given!(:med) { create(:course, name: 'Ciências Médicas', university: ufpe) }
  given!(:mat) { create(:course, name: 'Matemática', university: ufpb) }
  given!(:enf) { create(:course, name: 'Enfermagem', university: ufpi) }
  given!(:fis) { create(:course, name: 'Fisica', university: ufpb) }

  given!(:performance_ufpi) { create(:performance_evaluation, evaluable: ufpi, score: 10) }
  given!(:performance_ufpe) { create(:performance_evaluation, evaluable: ufpe, score: 100) }
  given!(:performance_ufpb) { create(:performance_evaluation, evaluable: ufpb, score: 60) }
  given!(:performance_bcc) { create(:performance_evaluation, evaluable: bcc, score: 0, score_course: 39.89, average_score_student: 40.0) }
  given!(:performance_med) { create(:performance_evaluation, evaluable: med, score: 0, score_course: 99.99, average_score_student: 99.0) }
  given!(:performance_mat) { create(:performance_evaluation, evaluable: mat, score: 0, score_course: 24.09, average_score_student: 30.76) }
  given!(:performance_enf) { create(:performance_evaluation, evaluable: enf, score: 0, score_course: 54.90, average_score_student: 60.90) }
  given!(:performance_fis) { create(:performance_evaluation, evaluable: fis, score: 0, score_course: 4, average_score_student: 9) }

  scenario 'search performance evaluations' do
    visit '/'

    within 'table' do
      within 'tbody tr:nth-child(1)' do
        expect(page).to have_content 'UFPE - 100.0'
      end

      within 'tbody tr:nth-child(2)' do
        expect(page).to have_content 'UFPB - 60.0'
      end

      within 'tbody tr:nth-child(3)' do
        expect(page).to have_content 'UFPI - 10.0'
      end

      within 'tbody tr:nth-child(4)' do
        expect(page).to have_content 'UFPE Ciências Médicas - 99.99 99.0'
      end

      within 'tbody tr:nth-child(5)' do
        expect(page).to have_content 'UFPI Enfermagem - 54.9 60.9'
      end
    end
  end
end
