FactoryGirl.define do
  factory :performance_evaluation do
    association :evaluable, factory: :university
    score { rand(1..999) }
  end
end
