FactoryGirl.define do
  factory :course do
    university
    name 'Ciência da Computação'
  end
end
