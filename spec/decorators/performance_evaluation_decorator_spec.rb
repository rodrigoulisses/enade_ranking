require 'rails_helper'

RSpec.describe PerformanceEvaluationDecorator do
  describe '#university' do
    context 'when subject is university' do
      let(:university) { build(:university) }
      subject { build(:performance_evaluation, evaluable: university).decorate }

      it 'returns evaluable' do
        expect(subject.university).to eql university.name
      end
    end

    context 'when subject is course' do
      let(:course) { build(:course) }

      subject { build(:performance_evaluation, evaluable: course).decorate }

      it 'returns university course' do
        expect(subject.university).to eql course.university.name
      end
    end
  end

  describe '#course' do
    context 'when subject is university' do
      let(:university) { build(:university) }
      subject { build(:performance_evaluation, evaluable: university).decorate }

      it 'returns -' do
        expect(subject.course).to eql '-'
      end
    end

    context 'when subject is course' do
      let(:course) { build(:course) }

      subject { build(:performance_evaluation, evaluable: course).decorate }

      it 'returns course name' do
        expect(subject.course).to eql course.name
      end
    end
  end

  describe '#score' do
    context 'when subject is university' do
      let(:university) { build(:university) }
      subject { build(:performance_evaluation, evaluable: university, score: 50).decorate }

      it 'returns score' do
        expect(subject.score).to eql 50.0
      end
    end

    context 'when subject is course' do
      let(:course) { build(:course) }

      subject { build(:performance_evaluation, evaluable: course).decorate }

      it 'returns -' do
        expect(subject.score).to eql '-'
      end
    end
  end

  describe '#score_course' do
    context 'when subject is university' do
      let(:university) { build(:university) }

      subject { build(:performance_evaluation, evaluable: university, score: 50).decorate }

      it 'returns -' do
        expect(subject.score_course).to eql '-'
      end
    end

    context 'when subject is course' do
      let(:course) { build(:course) }

      subject { build(:performance_evaluation, evaluable: course, score_course: 40).decorate }

      it 'returns score_course' do
        expect(subject.score_course).to eql 40.0
      end
    end
  end

  describe '#average_score_student' do
    context 'when subject is university' do
      let(:university) { build(:university) }

      subject { build(:performance_evaluation, evaluable: university, score: 50).decorate }

      it 'returns -' do
        expect(subject.average_score_student).to eql '-'
      end
    end

    context 'when subject is course' do
      let(:course) { build(:course) }

      subject { build(:performance_evaluation, evaluable: course, score_course: 40, average_score_student: 30).decorate }

      it 'returns average_score_student' do
        expect(subject.average_score_student).to eql 30.0
      end
    end
  end
end
