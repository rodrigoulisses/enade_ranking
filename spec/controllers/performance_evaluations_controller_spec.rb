require 'rails_helper'

RSpec.describe PerformanceEvaluationsController, type: :controller do
  let!(:ufpi) { create(:university, name: 'UFPI') }
  let!(:ufpe) { create(:university, name: 'UFPE') }
  let!(:ufpb) { create(:university, name: 'UFPB') }

  let!(:bcc) { create(:course, name: 'Ciência da Computação', university: ufpi) }
  let!(:med) { create(:course, name: 'Ciências Médicas', university: ufpe) }
  let!(:mat) { create(:course, name: 'Matemática', university: ufpb) }
  let!(:enf) { create(:course, name: 'Enfermagem', university: ufpi) }
  let!(:fis) { create(:course, name: 'Fisica', university: ufpb) }

  let!(:performance_ufpi) { create(:performance_evaluation, evaluable: ufpi, score: 10) }
  let!(:performance_ufpe) { create(:performance_evaluation, evaluable: ufpe, score: 100) }
  let!(:performance_ufpb) { create(:performance_evaluation, evaluable: ufpb, score: 60) }
  let!(:performance_bcc) { create(:performance_evaluation, evaluable: bcc, score_course: 39.89, average_score_student: 40.0) }
  let!(:performance_med) { create(:performance_evaluation, evaluable: med, score_course: 99.99, average_score_student: 99.0) }
  let!(:performance_mat) { create(:performance_evaluation, evaluable: mat, score_course: 24.09, average_score_student: 30.76) }
  let!(:performance_enf) { create(:performance_evaluation, evaluable: enf, score_course: 54.90, average_score_student: 60.90) }
  let!(:performance_fis) { create(:performance_evaluation, evaluable: fis, score_course: 4, average_score_student: 9) }

  describe '#GET index' do
    before do
      get :index, params: params
    end

    context 'when find by university' do
      let(:params) do
        { q: { university_name_like: 'UFPI' } }
      end

      it 'includes performance where university name like UFPI' do
        expect(assigns(:performance_evaluations)).to include performance_ufpi 
      end

      it 'does not include performance where university name doens`t like UFPI' do
        expect(assigns(:performance_evaluations)).not_to include performance_ufpe, performance_ufpb 
      end
    end

    context 'when find by course name' do
      let(:params) do
        { q: { course_name_like: 'Ciência' } }
      end

      it 'includes performance where course name like Ciência' do
        expect(assigns(:performance_evaluations)).to include performance_bcc, performance_med
      end

      it 'does not include performance where course name doens`t like Ciência' do
        expect(assigns(:performance_evaluations)).not_to include performance_enf, performance_fis, performance_mat 
      end
    end
    
    context 'when find by score_gteq' do
      context 'when gteq is equal to 70' do
        let(:params) do
          { q: { score_gteq: '70' } }
        end
        it 'includes performances with score greater than and equal to 70' do
          expect(assigns(:performance_evaluations)).to include(performance_ufpe)
        end

        it 'does not includes performances with score less than to 70' do
          expect(assigns(:performance_evaluations)).not_to include(performance_ufpi, performance_ufpb)
        end
      end
    end

    context 'when find by score_lteq' do
      context 'when score_lteq is equal to 70' do
        let(:params) do
          { q: { score_lteq: '70' } }
        end

        it 'does includes performances with score less than and equal to 70' do
          expect(assigns(:performance_evaluations)).not_to include(performance_ufpe)
        end

        it 'includes performances with score greater than to 70' do
          expect(assigns(:performance_evaluations)).to include(performance_ufpi, performance_ufpb)
        end
      end
    end

    context 'when find by score_course_gteq' do
      context 'when score_course_gteq is equal to 50' do
        let(:params) do
          { q: { score_course_gteq: '50' } }
        end
        it 'includes performances with score_course greater than and equal to 50' do
          expect(assigns(:performance_evaluations)).to include(performance_med, performance_enf)
        end

        it 'does not includes performances with score less than to 50' do
          expect(assigns(:performance_evaluations)).not_to include(performance_bcc, performance_mat, performance_fis)
        end
      end
    end

    context 'when find by score_lteq' do
      context 'when score_lteq is equal to 50' do
        let(:params) do
          { q: { score_course_lteq: '50' } }
        end

        it 'does includes performances with score less than and equal to 50' do
          expect(assigns(:performance_evaluations)).not_to include(performance_med, performance_enf)
        end

        it 'includes performances with score greater than 50' do
          expect(assigns(:performance_evaluations)).to include(performance_bcc, performance_mat, performance_fis)
        end
      end
    end

    context 'when find by average_score_student_gteq' do
      context 'when average_score_student_gteq is equal to 40' do
        let(:params) do
          { q: { average_score_student_gteq: 40 } }
        end

        it 'includes performances with average_score_student greater than and equal to 40' do
          expect(assigns(:performance_evaluations)).to include(performance_bcc, performance_med, performance_enf)
        end

        it 'includes performances with average_score_student less than 40' do
          expect(assigns(:performance_evaluations)).not_to include(performance_mat, performance_fis)
        end
      end
    end

    context 'when find by average_score_student_lteq' do
      context 'when average_score_student_lteq is equal to 40' do
        let(:params) do
          { q: { average_score_student_lteq: 40 } }
        end

        it 'includes performances with average_score_student less than and equal to 40' do
          expect(assigns(:performance_evaluations)).to include(performance_mat, performance_fis)
        end

        it 'does not includes performances with average_score_student greater than 40' do
        end
      end
    end
  end
end
