Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root 'performance_evaluations#index'

  resources :performance_evaluations, only: :index
end
